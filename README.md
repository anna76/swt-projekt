# UML to PlantUML Converter

The UML to PlantUML Converter is a program that converts UML files to PlantUML format.

## Prerequisites

Before running the UML to PlantUML Converter, ensure that you have the following dependencies:

- Java Development Kit (JDK) installed on your machine.
- Eclipse Modeling Framework (EMF) library and Papyrus tool for handling UML models.
- The required UML and EMF libraries in your project's classpath.

## Usage

1. Import the necessary libraries into your project, including `java.lang.reflect.Parameter`, `java.util`, and the EMF and UML-related packages.
2. Locate the `start` class in your code, which serves as the entry point for the converter.
3. Run the the `start.java`class.
4. You will be prompted to enter the path of the UML file you want to convert in the terminal. Provide the correct path and press Enter.
5. The program will load the UML model from the specified file and analyze its contents.
6. The program will display the analyzed elements of the UML model, including classes, associations, generalizations, realizations, dependencies, and usages in the terminal.

## Dependencies

The UML to PlantUML Converter relies on the following dependencies:

- EMF Core library (`org.eclipse.emf.ecore`)
- UML2 library (`org.eclipse.uml2.uml`)

Make sure to include these dependencies in your project's classpath.

## PlantUML
![UML to PlantUML Converter](images/Testlook.png)
