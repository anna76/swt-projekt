import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.AssociationClass;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.VisibilityKind;
import org.eclipse.uml2.uml.resources.util.UMLResourcesUtil;
import org.eclipse.uml2.uml.internal.resource.UMLResourceFactoryImpl;
import org.eclipse.uml2.uml.resource.UMLResource;

@SuppressWarnings("restriction")
public class start {

	static String verbindung = "";
	public static URI getUri() {
		System.out.print("Input file path: ");
		Scanner url = new Scanner(System.in);
		URI uri = URI.createURI(url.nextLine());
		return uri;
	}
	
	private static void getResource() {
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getPackageRegistry().put(UMLPackage.eNS_URI, UMLPackage.eINSTANCE);
		UMLResourcesUtil.init(resourceSet);
		
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(UMLResource.FILE_EXTENSION, UMLResource.Factory.INSTANCE);
	 	resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("uml", new UMLResourceFactoryImpl());
	 	
	 	try {
	 	    Resource resource = resourceSet.getResource(getUri(), true);
	 	    EcoreUtil.resolveAll(resourceSet);
	 	    Model uml_model = (Model) resource.getContents().get(0);
	 	
   	 
   	     
		for (Element element : uml_model.getOwnedElements()) {
   	 		System.out.println(element);
   	 	}
		
		uml_to_puml(uml_model);
   	 	
   	 	puml_to_uml("@startuml\r\n"
   	 			+ "enum Sterne {\r\n"
   	 			+ "1\r\n"
   	 			+ "2\r\n"
   	 			+ "3\r\n"
   	 			+ "4\r\n"
   	 			+ "5\r\n"
   	 			+ "}\r\n"
   	 			+ " ... ", resource);
	 	} catch (Exception e) {
	 	    System.out.println("Error loading resource because the file does not exist. Please try again with a correct file path");
	 	    getResource();
	 	}

	}
	public static void main(String[] args) {
		
		getResource();
   	 	
	}

	private static void uml_to_puml(org.eclipse.uml2.uml.Model model) {
		
		System.out.println("\nAnalyze Model:");
		System.out.println(model.getName());
		
		System.out.println("\n@startuml");
		
		for (PackageableElement packageableElement : model.getPackagedElements()) {
			if (packageableElement instanceof org.eclipse.uml2.uml.Class) {
				org.eclipse.uml2.uml.Class clazz =  (org.eclipse.uml2.uml.Class) packageableElement;
				System.out.println(uml_class_to_puml_class(clazz));
				getBeziehung(clazz);
			}
			if(packageableElement instanceof org.eclipse.uml2.uml.Enumeration) {
				org.eclipse.uml2.uml.Enumeration enumm = (org.eclipse.uml2.uml.Enumeration)packageableElement;
				System.out.println(uml_enum_to_puml_enum(enumm));
		
			}
			if(packageableElement instanceof org.eclipse.uml2.uml.Interface) {
				org.eclipse.uml2.uml.Interface interfaces = (org.eclipse.uml2.uml.Interface) packageableElement;
				System.out.print(uml_interface_to_puml_interface(interfaces));
			}
			if(packageableElement instanceof org.eclipse.uml2.uml.Association) {
				org.eclipse.uml2.uml.Association ass = (org.eclipse.uml2.uml.Association) packageableElement;
				//System.out.println(ass);
				getAssociation(ass);
			}
			if(packageableElement instanceof org.eclipse.uml2.uml.Generalization) {
				org.eclipse.uml2.uml.Generalization gen = (org.eclipse.uml2.uml.Generalization) packageableElement;
				System.out.println(gen);
			}
			if(packageableElement instanceof org.eclipse.uml2.uml.Realization) {
				org.eclipse.uml2.uml.Realization rel = (org.eclipse.uml2.uml.Realization) packageableElement;
				System.out.print(getRealization(rel));
			}
			if(packageableElement instanceof org.eclipse.uml2.uml.Dependency) {
				org.eclipse.uml2.uml.Dependency depend = (org.eclipse.uml2.uml.Dependency) packageableElement;
				System.out.print(getDependency(depend));
			}
			if(packageableElement instanceof org.eclipse.uml2.uml.Usage) {
				org.eclipse.uml2.uml.Usage use = (org.eclipse.uml2.uml.Usage) packageableElement;
				System.out.print(getUsage(use));
			}
			
				
		}
		System.out.println(verbindung);   //Placed here so it will be printed at the end of all the classes
		System.out.println(members1);      //Placed here so it will be printed at the end of all the classes
		
		System.out.println("\n@enduml");
		
	}
	// Method to handle multiplicity string
	private static String getMultiplicityString(Property property) {
	    String multiplicityString = "";

	    // Handling lower multiplicity
	    int lower = property.getLower();
	    if (lower == 0) {
	        multiplicityString += "\""+ "0" ;
	    } else if (lower == 1) {
	        multiplicityString += "\""+ "1";	
	    }else if(lower > 0 ){
	        multiplicityString += "\"" +lower ;
	    }
	    // Handling upper multiplicity
	    if(property.getUpper() != 0 ) {
		    int upper = property.getUpper();
		    if (upper < 0) {
		        multiplicityString += "..*" + "\"";
		    } else if (upper >= 1) {
		        multiplicityString += ".." + upper + "\"";
		    } 
	    }
	    if(((Integer)property.getUpper()).toString().equals(null))multiplicityString +=  "\"";

	    return multiplicityString ;
	}
	//file:/C:\\Users\\splas\\OneDrive\\Desktop\\SWTP\\Block4\\Block4\\model\\Block3.uml

	//Getting AssociationName 
	private static void AssociationName(Property property) {
		Association association = property.getAssociation();
        String associationClasses = "";
        String associationName = association.getName();
        String s = "";
        String associationType = association.isBinary() ? " -- " : " ()-- ";
        	if(!(property instanceof org.eclipse.uml2.uml.Class)) {
                if (associationClasses.isEmpty() && associationName != null) 
                	verbindung  += ":" + associationName + "\n";
                	
                }
        	verbindung += "\n";
	}
	
	//Method to get the relation between an interface and a class
	//Themethod covers both Reslisation and Usage
		private static String getRealization(org.eclipse.uml2.uml.Realization rel) {
	    String members = "";
	    NamedElement clients = rel.getClients().get(0);
	    NamedElement interfaze = rel.getSuppliers().get(0);
	    members += "\n"  + clients.getName()+ "()" + "-" + interfaze.getName() + "\n";	   
	    return members;
	}
	private static String getUsage(org.eclipse.uml2.uml.Usage use) {
	    String members = "";
	    NamedElement client = use.getClients().get(0);
	    NamedElement interfaze = use.getSuppliers().get(0);
	    members += "\n"  + client.getName()+ "()" + "-" + interfaze.getName() + "\n";   
	    return members;
	}
	private static String getDependency(org.eclipse.uml2.uml.Dependency depend) {
		String members = "";
		if(!(depend instanceof org.eclipse.uml2.uml.Usage || depend instanceof org.eclipse.uml2.uml.Realization)) {
			NamedElement client = depend.getClients().get(0);
			NamedElement interfaze = depend.getSuppliers().get(0);
			members += "\n"  + client.getName()+  "..>" + interfaze.getName();
			if(depend.getName() != null)members += ":"+ depend.getName();
			members += "\n";  
		} 
		return members;
	}
	
	private static Association findAssociationById(Model model, String associationId) {
	    for (Element element : model.getOwnedElements()) {
	        if (element instanceof Association) {
	            String elementId = element.eResource().getURIFragment(element);
	            System.out.println("ID: " + elementId);
	            if (elementId != null && elementId.equals(associationId)) {
	                return (Association) element;
	            }
	        }
	    }
	    return null; // Association with the specified ID not found
	}

	//Handling of simple not directed associations

	static String members1 = "";
	static Set<String> processedAssociations = new HashSet<>();
	static String existingEnd1 = "";
	static String existingEnd2 = "";

	private static void getAssociation(org.eclipse.uml2.uml.Association asso) {
	    EList<Property> list = asso.getOwnedEnds();

	    if (list.isEmpty()) {
	        // Association without owned ends
	        // Retrieve related classes from the association itself
	        List<Classifier> relatedClasses = asso.getRedefinedClassifiers();
	        if (!relatedClasses.isEmpty()) {
	            String associationString = "";
	            for (int i = 1; i < relatedClasses.size(); i++) {
	                if (i > 1) {
	                    associationString += ",";
	                }
	                associationString += relatedClasses.get(i).getName();
	            }
	            String association = relatedClasses.get(0).getName() + "--" + "(" + associationString + ")";
	            if (asso.getName() != null) {
	                association += ":" + asso.getName();
	            }
	            members1 += association + "\n";
	        }
	    } else {
	        // Association with owned ends
	        boolean hasNonNullOwnedEnd = false;
	        Property ownedEnd1 = null;
	        Property ownedEnd2 = null;

	        for (Property ass : asso.getOwnedEnds()) {
	            AggregationKind ag = ass.getAggregation();
	            String ownedEndName = ass.getType().getName();
	            String associationName = asso.getName();

	            if (ag != null && !processedAssociations.contains(ownedEndName) && ag.equals(AggregationKind.NONE_LITERAL)) {
	                processedAssociations.add(ownedEndName);

	                Property otherEnd = ass.getOtherEnd();

	                if (otherEnd != null) {
	                    hasNonNullOwnedEnd = true;

	                    if (ownedEnd1 == null) {
	                        ownedEnd1 = ass;
	                        ownedEnd2 = otherEnd;
	                    } else {
	                        String otherEndName = otherEnd.getType().getName();
	                        String pair = ownedEndName + "--" + otherEndName;

	                        // Check if the pair has already been processed in the opposite order
	                        if (!processedAssociations.contains(pair) && !processedAssociations.contains(otherEndName + "--" + ownedEndName)) {
	                            String associationKey = ownedEndName + "--" + otherEndName;
	                            // Checks if the associationKey has already been processed
	                            if (!processedAssociations.contains(associationKey)) {
	                                String updatedAssociation = "";
	                                if (ownedEnd1.getType().getName() == null && ownedEnd2.getType().getName() != null) {
	                                    updatedAssociation = "(" + existingEnd1 + "," + existingEnd2 + ")" + "--" + ownedEndName;
	                                }
	                                if (ownedEnd1.getType().getName() != null && ownedEnd2.getType().getName() == null) {
	                                    updatedAssociation = "(" + existingEnd2 + "," + existingEnd1 + ")" + "--" + ownedEndName;
	                                }

	                                if (!updatedAssociation.isEmpty()) {
	                                    if (asso.getName() != null) {
	                                        updatedAssociation += ":" + asso.getName();
	                                    }
	                                    members1 = members1.replace(existingEnd1 + " -- " + existingEnd2, "");
	                                }

	                                members1 += updatedAssociation + "\n";
	                                // Update ownedEnd1 and ownedEnd2 for subsequent associations
	                                ownedEnd1 = ass;
	                                ownedEnd2 = otherEnd;
	                            }
	                        }
	                    }
	                }
	            }
	        }

	        if (hasNonNullOwnedEnd) {
	            // Update existingEnd1 and existingEnd2
	            existingEnd1 = ownedEnd1 != null ? ownedEnd1.getType().getName() : "";
	            existingEnd2 = ownedEnd2 != null ? ownedEnd2.getType().getName() : "";
	        }

	        String associationKey = "";
	        if (!hasNonNullOwnedEnd) {
	            // Association with one owned end null and the other not
	            String association = "";
	            if (ownedEnd1 != null && ownedEnd2 != null) {
	                String ownedEnd1Name = ownedEnd1.getType().getName();
	                String ownedEnd2Name = ownedEnd2.getType().getName();
	                String pair = ownedEnd1Name + "--" + ownedEnd2Name;

	                // Check if the pair has already been processed in the opposite order
	                if (!processedAssociations.contains(pair) && !processedAssociations.contains(ownedEnd2Name + "--" + ownedEnd1Name)) {
	                    associationKey = ownedEnd1Name + "--" + ownedEnd2Name;
	                    // Checks if the associationKey has already been processed
	                    if (!processedAssociations.contains(associationKey)) {
	                        association = ownedEnd1Name + "--" + ownedEnd2Name;
	                        if (asso.getName() != null) {
	                            association += ":" + asso.getName();
	                        }
	                        members1 += association + "\n";
	                    }
	                }
	            }
	        }

	        if (hasNonNullOwnedEnd) {
	            // Update existingEnd1 and existingEnd2
	            existingEnd1 = ownedEnd1 != null ? ownedEnd1.getType().getName() : "";
	            existingEnd2 = ownedEnd2 != null ? ownedEnd2.getType().getName() : "";

	            // Handle the case where both ends are present and have names
	            if (ownedEnd1 != null && ownedEnd2 != null && ownedEnd1.getType().getName() != null && ownedEnd2.getType().getName() != null) {
	                String association = ownedEnd1.getType().getName() + "--" + ownedEnd2.getType().getName();
	                associationKey = ownedEnd1.getType().getName() + "--" + ownedEnd2.getType().getName();
	                boolean isAlreadyProcessed = processedAssociations.contains(associationKey);
	                boolean hasRelationshipWithAnotherClass = false;

	                // Check if the associationKey has already been processed or has a relationship with another class
	                if (!isAlreadyProcessed) {
	                    // Check if there is a relationship with another class
	                    for (String processedAssociation : processedAssociations) {
	                        if (processedAssociation.startsWith("(" + ownedEnd1.getType().getName() + "," + ownedEnd2.getType().getName()) ||
	                                processedAssociation.startsWith("(" + ownedEnd2.getType().getName() + "," + ownedEnd1.getType().getName())) {
	                            hasRelationshipWithAnotherClass = true;
	                            break;
	                        }
	                    }
	                }

	                // Checks if the associationKey has already been processed or has a relationship with another class
	                if (!isAlreadyProcessed && !hasRelationshipWithAnotherClass) {
	                	if(asso.getName() != null) {
	                		association += ":" + asso.getName();
	                	}
	                    members1 += association + "\n";
	                    processedAssociations.add(associationKey);
	                }
	            }
	            
	            // Handle reflexive relations (Class1--Class1)
	            if (ownedEnd1 != null && ownedEnd2 != null && ownedEnd1.getType() == ownedEnd2.getType()) {
	                String association = ownedEnd1.getType().getName() + "--" + ownedEnd2.getType().getName();
	                if (!processedAssociations.contains(association)) {
	                    if (asso.getName() != null) {
	                        association += ":" + asso.getName();
	                    }
	                    members1 += association + "\n";
	                    processedAssociations.add(association);
	                }
	            }
	        }
	    }
	}




	       


	private static void getBeziehung(org.eclipse.uml2.uml.Class clazz) {
	    for (Property property : clazz.getAllAttributes()) {
	    	String simpleAsso = "";
	        if (property.getAssociation() != null) {
	            AggregationKind aggk = property.getAggregation();
	            if (aggk != null) {
	                // Composite association
	                if (aggk.equals(AggregationKind.COMPOSITE_LITERAL)) {
	                    verbindung += "\n" + clazz.getName() + " *-- " + getMultiplicityString(property) + property.getType().getName();
	                    simpleAsso = clazz.getName() + "--" + property.getType().getName();
                        processedAssociations.add(simpleAsso);
	                }
	                // Aggregation association
	                if (aggk.equals(AggregationKind.SHARED_LITERAL)) {
	                    verbindung += "\n" + clazz.getName() + " o-- " + getMultiplicityString(property) + property.getType().getName();
	                    simpleAsso = clazz.getName() + "--" + property.getType().getName();
                        processedAssociations.add(simpleAsso);
	                }
	                // Non-composite and non-aggregation association (Directed association)
	                if (aggk.equals(AggregationKind.NONE_LITERAL)) {
	                    if (property.getAssociation().getMemberEnds().size() == 2) {
	                            verbindung += clazz.getName() + " --> " + getMultiplicityString(property) + property.getType().getName();
	                            simpleAsso = clazz.getName() + "--" + property.getType().getName();
	                            processedAssociations.add(simpleAsso);
	                        }  
	                } else if (property.getAssociation().getMemberEnds().size() == 1) {
                        // Reflexive relation
                        verbindung += clazz.getName() + " <|-- " + getMultiplicityString(property) + clazz.getName();
                        simpleAsso = clazz.getName() + "<--" + clazz.getName();
                        processedAssociations.add(simpleAsso);
                    }
	            }
                AssociationName(property);
	        }
	        
	    }
	    //Generalization
	    for (Classifier superClass : clazz.getGenerals()) {
	        if (superClass instanceof org.eclipse.uml2.uml.Class) {
	            verbindung += clazz.getName() +" --|> " +  superClass.getName() + "\n";
	        }
			//AssociationName(property);
	    }
	}

	//Checking for ownedComments
	static 	String comment = "";
	private static void getComment(org.eclipse.uml2.uml.Class clazz) {
		comment = "";
		for(Property c : clazz.getAllAttributes()) {
			if(!c.getOwnedComments().isEmpty()) {
				comment += "\nnote left: " +  c.getOwnedComments().get(0).getBody() +  "\n";
			}
		}
	}
	//Method for getting the parameters of methods in classes #Joaquin
	private static String getParameters(org.eclipse.uml2.uml.Operation operation) {
		   String ret="";
		EList<org.eclipse.uml2.uml.Parameter> parameters = operation.getOwnedParameters();
		if(parameters.size()<2) {
			 ret=ret+"";
		}else {
		for (int i = 0; i < parameters.size(); i++) {
			org.eclipse.uml2.uml.Parameter parameter = parameters.get(i);
	  
			if( parameter.getName()==null ) {
			  ret=ret+"";
			}else {
			ret=ret+parameter.getName()+":"+parameter.getType().getName();
			}
			// Append a comma if it's not the last parameter
			if( parameters.size()< 2) 
			{ 
				if( parameters.size()== 1) {
					 ret=ret+parameter.getName()+":tototot"+parameter.getType().getName();
					 break;
				}else {
				ret=ret+"";
				break;
				}
			}
			if ((i < parameters.size() - 1)&&(parameters.size()>2)) {
				ret=ret+", ";
			}
		  } 
		}
		return ret;
   } 
	//To get the methods of classes
	//Have to implement something similar for interfaces
	private static String pumlMethods_to_uml(org.eclipse.uml2.uml.Class clazz) {
		String ret="";
			for (org.eclipse.uml2.uml.Operation operation : clazz.getAllOperations()) {
					VisibilityKind vk = operation.getVisibility();
					String visibility = operation.getVisibility().getLiteral();
					ret=ret+ clazz.getName() + " : ";
					if (visibility == "public") {
						ret = ret + "+";
					}
					else if(visibility == "private") {
						ret = ret + "-";
					}
					else if(visibility == "protected?:_literal") {
						ret = ret + "#";
					}
					else if(visibility == "protected") {
						ret = ret + "~";
					}
					ret = ret+operation.getName();
				
					ret=ret+"("+getParameters(operation)+"):";
					
					// ret+=getParameters(operation);
					if(operation.getType()== null) {
					
						ret = ret+"\n";
					}else {
					
		ret = ret+operation.getType().getName() + "\n";
					}
					// ret.append(":").append(operation.getType().getName());
				}

			return ret;
		}
    //Method checks for attributes as well as constructors of a class
	private static String uml_class_to_puml_class(org.eclipse.uml2.uml.Class clazz) {
	    StringBuilder ret = new StringBuilder();
		getComment(clazz);

	    // Write constructor
	    boolean hasConstructor = false;
	    VisibilityKind constructorVisibility = VisibilityKind.PRIVATE_LITERAL;
	    List<String> constructorParams = new ArrayList<>();

	    String className = clazz.isAbstract() ? "\nabstract class " + clazz.getName() : "\nclass " + clazz.getName();

	    // Write other properties
	    ret.append(className).append(" {\n");
	    for (Property property : clazz.getAllAttributes()) {
	        if (property.getAssociation() == null) {
	            VisibilityKind vk = property.getVisibility();
	            String propertySymbol;
	            if (vk.equals(VisibilityKind.PUBLIC_LITERAL)) {
	                propertySymbol = "+";
	            } else if (vk.equals(VisibilityKind.PRIVATE_LITERAL)) {
	                propertySymbol = "-";
	            } else if (vk.equals(VisibilityKind.PROTECTED_LITERAL)) {
	                propertySymbol = "#";
	            } else {
	                propertySymbol = "~"; // Default symbol for package-private visibility
	            }
	            ret.append("\t").append(propertySymbol).append(" ").append(property.getName()).append(":").append(property.getType().getName()).append("\n");
	        }
	    }

	    ret.append("}").append(comment);
	    String methods = pumlMethods_to_uml( clazz);
		if(methods.length()>4) {
		
		ret.append("\n" + methods + "\n");
		}else {
		
		}
	    return ret.toString();
	}
		// Begin of Enum and Interface
	private static String uml_enum_to_puml_enum(org.eclipse.uml2.uml.Enumeration enumm) {
		String ret = "";

		ret = ret + "\nenum " + enumm.getName() + " {\n";
		
		
		for (EnumerationLiteral literal : enumm.getOwnedLiterals()) {
			
			ret = ret + literal.getName() + "\n";
		}

		for (Property literal : enumm.getAllAttributes()) {
		
			ret = ret + literal.getName() + "\n";
		}

		ret = ret + "}";
		return ret;
	}
	//For interfaces
	private static String uml_interface_to_puml_interface(org.eclipse.uml2.uml.Interface umlInterface) {
	    String ret = "";

	    ret = ret + "\ninterface " + umlInterface.getName() + " {\n";

	    for (Operation operation : umlInterface.getAllOperations()) {
	        // Darstellung von Operationen im PlantUML-Format
	        String visibility = getVisibilitySymbol(operation.getVisibility());
	        String returnType = operation.getType() != null ? operation.getType().getName() : "void";
	        ret = ret + visibility + operation.getName() + "() : " + returnType + "\n";
	    }
	    	

	    for (Property property : umlInterface.getAllAttributes()) {
	        // Darstellung von Attributen im PlantUML-Format
	    	 if (property.getAssociation() != null) {
				 ret = ret + "\n" + umlInterface.getName() + " -- " + ((Property) property.getAssociation().getMembers().get(1)).getType().getName() + "\n";
			 
	        String visibility = getVisibilitySymbol(property.getVisibility());
	        ret = ret + visibility + property.getName() + " : " + property.getType().getName() + "\n";
	    }
	    }

	    ret = ret + "}";
	    return ret;
	}
	//Get the visibility of interfaces
	private static String getVisibilitySymbol(VisibilityKind visibility) {
	    if (visibility.equals(VisibilityKind.PUBLIC_LITERAL)) {
	        return "+ ";
	    } else if (visibility.equals(VisibilityKind.PRIVATE_LITERAL)) {
	        return "- ";
	    } else if (visibility.equals(VisibilityKind.PROTECTED_LITERAL)) {
	        return "# ";
	    } else {
	        return "~ ";
	    }
	}
	
	
	private static void puml_to_uml(String puml, Resource resource) {
		
		/*
		UMLPackage.eINSTANCE.eClass();
		UMLFactory factory = UMLFactory.eINSTANCE;
		
		StateMachine statemachine = factory.createStateMachine();
		statemachine.setName("MyGeneratedStateMachine");
		
		Region region = factory.createRegion();
		region.setName("MyRegion");
		
		statemachine.getRegions().add(region);
		
		Pseudostate ps =  factory.createPseudostate();
		ps.setName("Initial1");
		
		region.getSubvertices().add(ps);
		
		State state = factory.createState();
		state.setName("On");
		
		region.getSubvertices().add(state);
		
		Transition transition = factory.createTransition();
		transition.setSource(ps);
		transition.setTarget(state);
		
		region.getTransitions().add(transition);
		
		NamedElement namedElement = uml_model.getOwnedMember("CoffeeMachineController");
		Operation operation_trigger = ((org.eclipse.uml2.uml.internal.impl.ClassImpl) namedElement).getOperation("switch_on_off", null, null);
				
		Operation operation_effect = ((org.eclipse.uml2.uml.internal.impl.ClassImpl) namedElement).getOperation("set_on", null, null);
		
		CallEvent call_event = factory.createCallEvent();
		call_event.setOperation(operation_trigger);
		
		Trigger trigger = factory.createTrigger();
		trigger.setEvent(call_event);
		
		Behavior behavior = factory.createOpaqueBehavior();
		behavior.setSpecification(operation_effect);
		
		
		transition.getTriggers().add(trigger);
		transition.setEffect(behavior);
		
		uml_model.getPackagedElements().add(call_event);
		
		uml_model.getPackagedElements().add(statemachine);
				
	    try {
	           resource.save(Collections.EMPTY_MAP);
	    } catch (IOException e) {
	           // TODO Auto-generated catch block
	           e.printStackTrace();
	    }
		*/
	}

}
